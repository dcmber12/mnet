import argparse
import os
import shutil
import time

import torch
import torch.nn as nn
import torch.nn.parallel
import torch.utils.data
import torch.nn.functional
import torchvision.transforms as transforms
import torchvision.datasets as datasets
import torch.backends.cudnn as cudnn
import torch.optim as optim
import model.resnet_base as resnet_base
# from tensorboard_logger import configure, log_value
from tensorboardX import SummaryWriter
from util import AverageMeter
from util import CustomCIFAR10
import shutil

parser = argparse.ArgumentParser(description='CIFAR10 experiment')
parser.add_argument('--epochs', default=1000, type=int)
parser.add_argument('--start-epoch', default=400, type=int,
                    help='when resume training start-epoch is changed')
parser.add_argument('--batch_size', default=128, type=int)
parser.add_argument('--lr', default=0.2, type=float)
parser.add_argument('--lr-decay', type=float, default=0.1)
parser.add_argument('--lr-decay-rate', type=float, default=150)
parser.add_argument('--weight-decay', type=float, default=1e-4)
parser.add_argument('--optimizer', type=str, default='momentum_sgd')
parser.add_argument('--momentum', default=0.9, type=float, help='momentum')
parser.add_argument('--print_interval_train', default=60, type=int)
parser.add_argument('--print_interval_test', default=10, type=int)
parser.add_argument('--no-augment', dest='augment', action='store_false')
parser.add_argument('--resume', default='', type=str,
                    help='path to latest checkpoint (default: none)')
parser.add_argument('--output_num', default=6, type=int,
                    help='number of output classes')
parser.add_argument('--name', default='ResNet20CIFAR10_0425_n6_decoder', type=str,
                    help='name of experiment')


def main():
    args = parser.parse_args()
    if os.path.isdir('runs/%s/log' % args.name):
        shutil.rmtree('runs/%s/log' % args.name)
    writer = SummaryWriter(log_dir='runs/%s/log' % args.name)

    args.resume = 'runs/ResNet20CIFAR10_0425_n6/model_best.pth.tar'
    args.decoder_loss_coef = 10

    # ---------------------------------------------------------------
    # Data Loading
    # ---------------------------------------------------------------
    normalize = transforms.Normalize(mean=[x / 255.0 for x in [125.3, 123.0, 113.9]],
                                     std=[x / 255.0 for x in [63.0, 62.1, 66.7]])

    if args.augment:
        transform_train = transforms.Compose([
            transforms.RandomCrop(32, padding=4),
            transforms.RandomHorizontalFlip(),
            transforms.ToTensor(),
            normalize,
        ])
    else:
        transform_train = transforms.Compose([
            transforms.ToTensor(),
            normalize,
        ])
    transform_test = transforms.Compose([
        transforms.ToTensor(),
        normalize
    ])

    kwargs = {'num_workers': 1, 'pin_memory': True}
    label_subset = [0, 1, 2, 3, 4, 5]
    assert len(label_subset) == args.output_num
    train_loader = torch.utils.data.DataLoader(
        CustomCIFAR10('data', label_subset=label_subset,
                      train=True, download=True,
                      transform=transform_train),
        batch_size=args.batch_size, shuffle=True,
        **kwargs)
    val_loader = torch.utils.data.DataLoader(
        CustomCIFAR10('data', label_subset=label_subset,
                      train=False,
                      transform=transform_test),
        batch_size=args.batch_size, shuffle=True,
        **kwargs)

    # ---------------------------------------------------------------
    # Get Model
    # ---------------------------------------------------------------
    # model = resnet_base.ResNet20CIFAR10(args)
    model = resnet_base.ResNet20CIFAR10Decoder(args, label_subset)
    model.cuda()
    print('Number of model parameters: {}'.format(
        sum([p.data.nelement() for p in model.parameters()])))

    best_prec1 = 0
    if args.resume:
        if os.path.isfile(args.resume):
            print("=> loading checkpoint '{}'".format(args.resume))
            checkpoint = torch.load(args.resume)
            args.start_epoch = checkpoint['epoch']
            best_prec1 = checkpoint['best_prec1']
            model.load_state_dict(checkpoint['state_dict'])
            print("=> loaded checkpoint '{}' (epoch {})"
                  .format(args.resume, checkpoint['epoch']))
        else:
            print("=> no checkpoint found at '{}'".format(args.resume))

    cudnn.benchmark = True

    # ---------------------------------------------------------------
    # Define Optimizer
    # ---------------------------------------------------------------
    optimizer = {
        'adam': optim.Adam(model.parameters(), lr=args.lr, weight_decay=args.weight_decay),
        'momentum_sgd': optim.SGD(model.parameters(), args.lr, momentum=0.9, weight_decay=args.weight_decay),
        'nesterov_sgd': optim.SGD(model.parameters(), args.lr, momentum=0.9, weight_decay=args.weight_decay, nesterov=True),
    }.get(args.optimizer)

    # ---------------------------------------------------------------
    # Train and Validation
    # ---------------------------------------------------------------
    for epoch in range(args.start_epoch, args.epochs):
        adjust_learning_rate(args, optimizer, epoch)

        # train for one epoch
        train_loss_nll, train_loss_mse, train_acc = train(args, train_loader, model, optimizer, epoch)

        # evaluate on validation set
        val_loss_nll, val_loss_mse, val_acc = validate(args, val_loader, model, best_prec1)

        # log to TensorBoard
        writer.add_scalars("nll_loss",
                           {'train_loss': train_loss_nll,
                            'val_loss': val_loss_nll},
                           epoch)
        writer.add_scalars("mse_loss",
                           {'train_loss': train_loss_mse,
                            'val_loss': val_loss_mse},
                           epoch)
        writer.add_scalars("accuracy",
                           {'train_acc': train_acc,
                            'val_acc': val_acc},
                           epoch)

        # remember best prec@1 and save checkpoint
        is_best = val_acc > best_prec1
        best_prec1 = max(val_acc, best_prec1)
        save_checkpoint(args, {
            'epoch': epoch + 1,
            'state_dict': model.state_dict(),
            'best_prec1': best_prec1,
        }, is_best)
    print('Best accuracy: ', best_prec1)


def train(args, train_loader, model, optimizer, epoch):
    """Train for one epoch on the training set"""
    batch_time = AverageMeter()
    nll_losses = AverageMeter()
    mse_losses = AverageMeter()
    top1 = AverageMeter()

    # switch to train mode
    model.train()

    end = time.time()
    for batch_idx, (input, target) in enumerate(train_loader):
        target = target.cuda(async=True)
        input = input.cuda()
        input_var = torch.autograd.Variable(input)
        target_var = torch.autograd.Variable(target)

        # compute output
        nll_loss, prec1, decoder_loss = model(input_var, target_var)

        # measure accuracy and record loss
        nll_losses.update(nll_loss.data[0], input.size(0))
        mse_losses.update(decoder_loss.data[0], input.size(0))
        top1.update(prec1[0], input.size(0))

        # compute gradient and do SGD step
        optimizer.zero_grad()
        nll_loss.backward(retain_graph=True)
        decoder_loss *= args.decoder_loss_coef
        decoder_loss.backward()
        optimizer.step()

        # measure elapsed time
        batch_time.update(time.time() - end)
        end = time.time()

        if batch_idx % args.print_interval_train == 0:
            print('Epoch: [{0}][{1}/{2}]\t'
                  'Time {batch_time.val:.3f} ({batch_time.avg:.3f})\t'
                  'NllLoss {nll_loss.val:.4f} ({nll_loss.avg:.4f})\t'
                  'MSELoss {mse_loss.val:.4f} ({mse_loss.avg:.4f})\t'
                  'Prec@1 {top1.val:.3f} ({top1.avg:.3f})\t'
                  'Lr {lr:.6f}'.format(
                      epoch, batch_idx, len(train_loader), batch_time=batch_time,
                      nll_loss=nll_losses, mse_loss=mse_losses, top1=top1, lr=optimizer.param_groups[0]['lr']))

    return nll_losses.avg, mse_losses.avg, top1.avg


def validate(args, val_loader, model, best_prec):
    """Perform validation on the validation set"""
    batch_time = AverageMeter()
    Nlllosses = AverageMeter()
    MSElosses = AverageMeter()
    top1 = AverageMeter()

    # switch to evaluate mode
    model.eval()

    end = time.time()
    for batch_idx, (input, target) in enumerate(val_loader):
        target = target.cuda(async=True)
        input = input.cuda()
        input_var = torch.autograd.Variable(input, volatile=True)
        target_var = torch.autograd.Variable(target, volatile=True)

        # compute output
        nll_loss, prec1, decoder_loss = model(input_var, target_var)

        # measure accuracy and record loss
        Nlllosses.update(nll_loss.data[0], input.size(0))
        MSElosses.update(decoder_loss.data[0], input.size(0))
        top1.update(prec1[0], input.size(0))

        # measure elapsed time
        batch_time.update(time.time() - end)
        end = time.time()

        if batch_idx % args.print_interval_test == 0:
            print('Test: [{0}/{1}]\t'
                  'Time {batch_time.val:.3f} ({batch_time.avg:.3f})\t'
                  'NllLoss {nll_loss.val:.4f} ({nll_loss.avg:.4f})\t'
                  'MSELoss {mse_loss.val:.4f} ({mse_loss.avg:.4f})\t'
                  'Prec@1 {top1.val:.3f} ({top1.avg:.3f})'.format(
                   batch_idx, len(val_loader), batch_time=batch_time,
                   nll_loss=Nlllosses, mse_loss=MSElosses,
                   top1=top1))

    best_prec = max(top1.avg, best_prec)
    print(' * Prec@1 {top1.avg:.3f}\t'
          'Best Prec@1 {best_prec:.3f}  \n'.format(top1=top1, best_prec=best_prec))
    return Nlllosses.avg, MSElosses.avg, top1.avg


def adjust_learning_rate(args, optimizer, epoch):
    lr = args.lr * (args.lr_decay ** (epoch // args.lr_decay_rate))
    for param_group in optimizer.param_groups:
        param_group['lr'] = lr


def save_checkpoint(args, state, is_best, filename='checkpoint.pth.tar'):
    """Saves checkpoint to disk"""
    directory = "runs/%s/" % args.name
    if not os.path.exists(directory):
        os.makedirs(directory)
    filename = directory + filename
    torch.save(state, filename)
    if is_best:
        shutil.copyfile(filename, 'runs/%s/' % args.name + 'model_best.pth.tar')


if __name__ == "__main__":
    main()
