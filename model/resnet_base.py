import torch
from torch import nn
import model


def get_blocks(in_ch, out_ch, num_blocks, downsample=None):
    blocks = list()
    # Downsize is either MaxPool or DownsizeResidualBlock

    if downsample == "pooling":
        blocks.append(nn.MaxPool2d(2))
        blocks.append(model.ResidualBlock(in_ch, out_ch))
    elif downsample == "stride":
        blocks.append(model.ResidualDownsizeBlock(in_ch, out_ch))
    else:
        blocks.append(model.ResidualBlock(in_ch, out_ch))

    for _ in range(num_blocks - 1):
        blocks.append(model.ResidualBlock(out_ch, out_ch))
    return nn.Sequential(*blocks)


class ResNet18ImageNet(nn.Module):

    def __init__(self, args):
        super(ResNet18ImageNet, self).__init__()

        self.args = args
        input_dim = 224
        downsample = 1

        # CONV1 img 224 X 224
        self.conv1 = nn.Conv2d(3, 64, 7, stride=2, bias=True)  # 112 X 112
        downsample *= 2

        # CONV2X max pool here
        self.conv2x = nn.Sequential(get_blocks(64, 64, num_blocks=2, downsample="pooling"))  # 56 X 56
        downsample *= 2

        # CONV3X
        self.conv3x = nn.Sequential(get_blocks(64, 128, num_blocks=2, downsample="stride"))  # 28 X 28
        downsample *= 2

        # CONV4X
        self.conv4x = nn.Sequential(get_blocks(128, 256, num_blocks=2, downsample="stride"))  # 14 X 14
        downsample *= 2

        # CONV5X
        self.conv5x = nn.Sequential(get_blocks(256, 512, num_blocks=2, downsample="stride"))  # 7 X 7
        downsample *= 2

        # avg pool, fc, softmax
        assert input_dim % downsample == 0
        self.avg_poool = nn.AvgPool2d(input_dim/downsample)
        self.fc = nn.Linear(512, 1000)
        self.softmax = torch.nn.Softmax(dim=1)

    def forward(self, x):
        x = self.conv1(x)
        x = self.conv2x(x)
        x = self.conv3x(x)
        x = self.conv4x(x)
        x = self.conv5x(x)
        x = self.avg_poool(x)
        x = x.view(-1, 512)
        y = self.fc(x)
        return self.softmax(y)


class ResNet20CIFAR10(nn.Module):

    def __init__(self, args):
        super(ResNet20CIFAR10, self).__init__()

        self.args = args
        input_dim = 32
        downsample = 1

        # CONV1 img 32 X 32
        self.conv1 = nn.Sequential(
            nn.Conv2d(3, 16, 3, stride=1, padding=1, bias=True),  # 32 X 32
            nn.BatchNorm2d(16),
            nn.ReLU()
        )

        # CONV2X max pool here
        self.conv2x = nn.Sequential(get_blocks(16, 16, num_blocks=3))  # 32 X 32

        # CONV3X
        self.conv3x = nn.Sequential(get_blocks(16, 32, num_blocks=3, downsample="stride"))  # 16 X 16
        downsample *= 2

        # CONV4X
        self.conv4x = nn.Sequential(get_blocks(32, 64, num_blocks=3, downsample="stride"))  # 8 X 8
        downsample *= 2

        # avg pool, fc, softmax
        assert input_dim % downsample == 0
        self.avg_poool = nn.AvgPool2d((int(input_dim/downsample), int(input_dim/downsample)))
        self.fc = nn.Linear(64, self.args.output_num)
        self.softmax = torch.nn.Softmax(dim=1)

    def forward(self, x):
        x = self.conv1(x)
        x = self.conv2x(x)
        x = self.conv3x(x)
        x = self.conv4x(x)
        x = self.avg_poool(x)
        x = x.view(-1, 64)
        y = self.fc(x)
        return self.softmax(y)


class ResNet20CIFAR10Decoder(nn.Module):

    def __init__(self, args, label_subset):
        super(ResNet20CIFAR10Decoder, self).__init__()

        self.args = args
        self.label_subset = label_subset
        self.nll = torch.nn.NLLLoss().cuda()
        self.mse = torch.nn.MSELoss().cuda()

        input_dim = 32
        downsample = 1

        # CONV1 img 32 X 32
        self.conv1 = nn.Sequential(
            nn.Conv2d(3, 16, 3, stride=1, padding=1, bias=True),  # 32 X 32
            nn.BatchNorm2d(16),
            nn.ReLU()
        )

        # CONV2X max pool here
        self.conv2x = nn.Sequential(get_blocks(16, 16, num_blocks=3))  # 32 X 32

        # CONV3X
        self.conv3x = nn.Sequential(get_blocks(16, 32, num_blocks=3, downsample="stride"))  # 16 X 16
        downsample *= 2

        # CONV4X
        self.conv4x = nn.Sequential(get_blocks(32, 64, num_blocks=3, downsample="stride"))  # 8 X 8
        downsample *= 2

        # CONV5X
        self.num_class_vector = 12
        self.conv5x = nn.Sequential(get_blocks(64, self.num_class_vector*self.args.output_num,
                                               num_blocks=3, downsample="stride"))  # 4 X 4
        downsample *= 2

        # avg pool, fc, softmax
        assert input_dim % downsample == 0

        self.avg_poool = nn.AvgPool2d((int(input_dim / downsample), int(input_dim / downsample)))

        self.fc = nn.Linear(self.num_class_vector*self.args.output_num, self.args.output_num)
        # self.fc_sep = [nn.Linear(self.num_class_vector, 1) for _ in range(self.args.output_num)]
        # self.class_vector_fc = nn.Linear(64*self.num_class_vector,
        #                                  self.args.output_num*self.num_class_vector)
        # self.classifier_fc = nn.Linear(self.args.output_num*self.num_class_vector,
        #                                self.args.output_num)

        # decoders
        self.decoders = [model.Decoder(self.args).cuda() for _ in range(self.args.output_num)]

        self.softmax = torch.nn.Softmax(dim=1)

    def forward(self, x, label):
        y = self.conv1(x)
        y = self.conv2x(y)
        y = self.conv3x(y)
        y = self.conv4x(y)
        y = self.conv5x(y)

        # forward decoder
        mse_loss = 0
        for class_ in self.label_subset:
            batch_axis_mask = label.eq(class_)
            batch_axis_index = batch_axis_mask.nonzero()[:, 0]
            per_class_input = y.index_select(dim=0, index=batch_axis_index)
            per_class_target = x.index_select(dim=0, index=batch_axis_index)
            decoder_out = self.decoders[class_](per_class_input)
            mse_loss += self.mse(decoder_out, per_class_target)

        # forward classifier
        y = self.avg_poool(y)
        y = y.view(-1, self.args.output_num * self.num_class_vector)
        y = self.fc(y)
        y = self.softmax(y)

        # return self.softmax(y), None
        return self.nll(y, label), self.accuracy(y.data, label.data, topk=(1,))[0], mse_loss

    def accuracy(self, output, target, topk=(1,)):
        """Computes the precision@k for the specified values of k"""
        maxk = max(topk)
        batch_size = target.size(0)

        _, pred = output.topk(maxk, 1, True, True)
        pred = pred.t()
        correct = pred.eq(target.view(1, -1).expand_as(pred))

        res = []
        for k in topk:
            correct_k = correct[:k].view(-1).float().sum(0)
            res.append(correct_k.mul_(100.0 / batch_size))
        return res


