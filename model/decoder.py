import torch.nn as nn


class Decoder(nn.Module):

    def __init__(self, args):
        super(Decoder, self).__init__()

        # space 4x4 => 32X32
        # ch 64 => 3
        self.decoder = nn.Sequential(

            # updsample 8X8
            nn.Conv2d(72, 32, 3, stride=1, padding=1),
            nn.ConvTranspose2d(32, 32, 2, stride=2, padding=0),
            nn.BatchNorm2d(32),

            # updsample 16X16
            nn.Conv2d(32, 32, 3, stride=1, padding=1),
            nn.ConvTranspose2d(32, 32, 2, stride=2, padding=0),
            nn.BatchNorm2d(32),

            # updsample 32X32
            nn.Conv2d(32, 16, 3, stride=1, padding=1),
            nn.ConvTranspose2d(16, 16, 2, stride=2, padding=0),
            nn.BatchNorm2d(16),

            nn.Conv2d(16, 3, 3, stride=1, padding=1)
        )

    def forward(self, x):
        return self.decoder(x)
