import torch
import torch.nn as nn
import torch.nn.functional as F


class ResidualBlock(nn.Module):

    def __init__(self, in_ch, out_ch):
        super(ResidualBlock, self).__init__()

        self.conv1 = nn.Conv2d(in_ch, out_ch, 3, stride=1, padding=1, bias=True)
        self.norm1 = nn.BatchNorm2d(out_ch)
        self.act1 = nn.ReLU(inplace=True)

        self.conv2 = nn.Conv2d(out_ch, out_ch, 3, stride=1, padding=1, bias=True)
        self.norm2 = nn.BatchNorm2d(out_ch)

    def forward(self, x):
        y = self.conv1(x)
        y = self.norm1(y)
        y = self.act1(y)
        y = self.conv2(y)
        y = self.norm2(y)
        return y + x


class ResidualDownsizeBlock(nn.Module):

    def __init__(self, in_ch, out_ch):
        super(ResidualDownsizeBlock, self).__init__()

        self.conv1 = nn.Conv2d(in_ch, out_ch, 3, stride=2, padding=1, bias=True)
        self.norm1 = nn.BatchNorm2d(out_ch)
        self.act1 = nn.ReLU(inplace=True)

        self.conv2 = nn.Conv2d(out_ch, out_ch, 3, stride=1, padding=1, bias=True)
        self.norm2 = nn.BatchNorm2d(out_ch)

        self.downsample = nn.Sequential(
            nn.Conv2d(in_ch, out_ch, 2, stride=2, padding=0, bias=False),
            nn.BatchNorm2d(out_ch)
        )

    def forward(self, x):
        y = self.conv1(x)
        y = self.norm1(y)
        y = self.act1(y)
        y = self.conv2(y)
        y = self.norm2(y)
        x = self.downsample(x)
        return y + x


class ResidualBottleneckBlock(nn.Module):

    def __init__(self):
        nn.Module.__init__(self)

    def forward(self, x):
        pass
